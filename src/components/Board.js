import React, { useState, useEffect } from 'react';
import { makeWinCombos, arr } from '../helper';
import Modal from './Modal';
import Timer from './Timer';

const Board = () => {
	const [boardArr, setBoardArr] = useState(arr(3 * 3));
	const [userSign, setUserSign] = useState('X');
	const [aiSign, setAiSign] = useState('O');
	const [mazeSize, setMazeSize] = useState(null);
	const [turn, setTurn] = useState(true);
	const [reset, setReset] = useState(true);
	const [winner, setWinner] = useState({
		show: false,
		winner: null,
		tie: false
	});
	const [error, setError] = useState({
		mazeSize: null,
	})

	useEffect(() => {
		setAiSign('O');
		setUserSign('X');
		setTurn(true);
		setMazeSize(null);
	}, [reset]);

	/* Run After User chooses its tiles */
	useEffect(() => {
		if (!checkWin(boardArr, turn ? userSign : aiSign) && !checkTie()) {
			!winner.winner && !turn && setTimeout(() => {
				turns(bestSpot(), aiSign);
				setTurn(!turn);
			}, 500);
		}
	}, [turn]);

	const handleClick = (e) => {
		/* Prevent clicking again  */
		if (e.target.innerHTML === "X" || e.target.innerHTML === "O" || !turn) {
			e.preventDefault();
			return;
		}
		e.target.innerHTML = userSign;
		e.target.classList.add('user-tile');
		let currentTile = parseInt(e.target.getAttribute('data-tile'));
		if (typeof boardArr[currentTile] === 'number') {
			turns(currentTile, userSign);
			setTurn(!turn);
		}
	};

	const huPlayer = userSign;
	const aiPlayer = aiSign;

	const bestSpot = () =>
		mazeSize == 3 ? minimaxForThree(boardArr, aiPlayer).index : minimaxForRandom(boardArr).index;


	/* For 3x3 Maze */
	const minimaxForThree = (newBoard, player) => {
		let availSpots = emptySquares();

		if (checkWin(newBoard, huPlayer)) {
			return { score: -10 };
		} else if (checkWin(newBoard, aiPlayer)) {
			return { score: 10 };
		} else if (availSpots.length === 0) {
			return { score: 0 };
		}
		let moves = [];
		for (let i = 0; i < availSpots.length; i++) {
			let move = {};
			move.index = newBoard[availSpots[i]];
			newBoard[availSpots[i]] = player;
			if (player == aiPlayer) {
				let result = minimaxForThree(newBoard, huPlayer);
				move.score = result.score;
			} else {
				let result = minimaxForThree(newBoard, aiPlayer);
				move.score = result.score;
			}

			newBoard[availSpots[i]] = move.index;

			moves.push(move);
		}

		let bestMove;
		if (player === aiPlayer) {
			let bestScore = -10000;
			for (let i = 0; i < moves.length; i++) {
				if (moves[i].score > bestScore) {
					bestScore = moves[i].score;
					bestMove = i;
				}
			}
		} else {
			let bestScore = 10000;
			for (let i = 0; i < moves.length; i++) {
				if (moves[i].score < bestScore) {
					bestScore = moves[i].score;
					bestMove = i;
				}
			}
		}

		return moves[bestMove];
	}


	/* For other than 3 Maze */
	const minimaxForRandom = (newBoard) => {
		let availSpots = emptySquares();
		let res;
		if (availSpots.length > 0) {
			while (true) {
				res = Math.floor(Math.random() * newBoard.length);
				if (newBoard[res] !== "X" && newBoard[res] !== "O") {
					break;
				} else {
					continue;
				};
			}
		}
		return { index: res };
	}

	// check if user won.
	const checkWin = (board, player) => {
		let plays = board.reduce((a, e, i) =>
			(e === player) ? a.concat(i) : a, []);
		let gameWon = null;
		let winCombos = makeWinCombos(mazeSize);
		for (let [index, win] of winCombos.entries()) {
			if (win.every(elem => plays.indexOf(elem) > -1)) {
				gameWon = { index: index, player: player };
				break;
			}
		}
		return gameWon;
	}

	const turns = (squareId, player) => {
		let newBoard = boardArr;
		newBoard[parseInt(squareId)] = player;
		setBoardArr(newBoard);
		if (!turn) {
			document.querySelector(`td[data-tile='${squareId}']`).innerHTML = aiSign;
			document.querySelector(`td[data-tile='${squareId}']`).classList.add('ai-tile');
		}
		let gameWon = checkWin(newBoard, player);
		if (gameWon) {
			setWinner({
				show: true,
				winner: gameWon.player === aiSign ? 'You did well, better luck next time.' : 'Congratulations ! You Won !',
				tie: false
			})
			setBoardArr(arr());
		}
	}

	const checkTie = () => {
		if (emptySquares().length === 0) {
			setWinner({
				show: true,
				winner: "Match Draw !",
				tie: true
			})
			return true;
		}
		return false;
	}

	/* Find Empty tiles in the board */
	const emptySquares = () => boardArr.filter(s => typeof s == 'number');

	const handleMazeSizeInput = (e) => {
		if (!(e.target.value && [3, 4, 5].includes(parseInt(e.target.value.trim())))) {
			setError({ ...error, mazeSize: "Input must be 3,4, or 5!" });
			return;
		}
		let n = parseInt(e.target.value);
		setMazeSize(n);
		setBoardArr(arr(n * n));
		setError({ ...error, mazeSize: null });
	}

	const handleSignChange = (e) => {
		let sign = e.target.value;
		setUserSign(sign);
		sign === 'O' ? setAiSign('X') : setAiSign('O');
	}

	/* Get timer minute based on Maze size */
	const getMin = () => {
		switch (mazeSize) {
			case 3:
				return 1;
			case 4:
				return 2;
			case 5:
				return 3;
		}
	}

	/* Time Exceeded User lost */
	const timeExceeded = () => {
		setWinner({
			show: true,
			winner: 'You Lost because time exceeded',
			tie: true
		});
	}

	const UserInput = <div className="maze-container">
		<div className='sign-wrapper'>
			<h1> Select a sign:</h1>{" "}
			<select onChange={handleSignChange}>
				<option value="X" defaultValue>X</option>
				<option value="O">O</option>
			</select>
		</div>
		<div className='size-wrapper'>
			<h1>Enter maze size to continue:</h1>{" "}
			<input type="number" placeholder='Enter size of the Maze' onChange={handleMazeSizeInput} />
		</div>
		<div className='error-wrapper'>
			<div className='error'>{error.mazeSize && error.mazeSize.trim()}</div>
		</div>
	</div>

	const BoardJsx = <div className="board-container">
		<table>
			<tbody>
				{arr(mazeSize).map((parentEle, parentIdx) => <tr key={parentIdx}>{arr(mazeSize).map((childEle, childIdx) => <td key={childIdx} data-tile={mazeSize * parentIdx + childIdx} onClick={handleClick}></td>)}</tr>)}
			</tbody>
		</table>
		<Modal setReset={setReset} text={winner.winner} show={winner.show} tie={winner.tie} />
	</div>

	return !mazeSize ? UserInput : <><Timer timeExceeded={timeExceeded} initialMinute={getMin()} />{BoardJsx}</>;
}

export default Board;