import React from 'react';

const Modal = ({ show, text }) => {

  return (
    <div id="open-modal" className={`modal-window ${show && 'show-modal'}`}>
      <div>
        <a onClick={() => location.reload()} href="#" title="Close" className="modal-close">Close</a>
        <h1>{text}</h1>
        <div className="reset-button">
          <button onClick={() => location.reload()}>Reset</button>
        </div>
      </div>
    </div>
  );
}

export default Modal;
