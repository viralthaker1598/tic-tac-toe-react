export const arr = (n) => Array.from(Array(n).keys());

export const makeWinCombos = (mazeSize) => {
	let winCombos = [];
	for (var i = 0; i < mazeSize; i++) {
		let tempArr = [];
		for (var j = 0; j < mazeSize; j++) {
			let currentNum = mazeSize * i + j;
			tempArr.push(currentNum);
		}
		winCombos.push(tempArr);
	}

	for (var i = 0; i < mazeSize; i++) {
		let tempArr = [];
		for (var j = 0; j < mazeSize; j++) {
			let currentNum = mazeSize * j + i;
			tempArr.push(currentNum);
		}
		winCombos.push(tempArr);
	}

	let tempArr = [];
	for (var i = 0; i < mazeSize; i++) {
		if (i === 0) {
			tempArr.push(i);
		}
		else {
			tempArr.push((mazeSize + 1) * i);
		}
	}
	winCombos.push(tempArr);

	tempArr = [];
	for (var i = 1; i <= mazeSize; i++) {
		tempArr.push((mazeSize - 1) * i);
	}
	winCombos.push(tempArr);
	return winCombos;
}

