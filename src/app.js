//  =================== import ===============
import React from 'react';
import ReactDOM from 'react-dom';
import Board from './components/Board';
import './styles/styles.scss';

//  ==================== CODE ================
const jsx = (
	<div className='root'>
		<h1 className='title'>Tic Tac Toe</h1>
		<Board />
	</div>
);

const renderApp = () => {
	ReactDOM.render(jsx, document.getElementById('app'));
};

renderApp();
